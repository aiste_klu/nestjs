import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateTaskDto } from './dto/create-task.dto';
import { Task } from './task.model';

@Injectable()
export class TaskService {
  private tasks: Task[] = [];

  constructor(@InjectModel('Task') private readonly TaskModel: Model<Task>) {}

  getAllTasks(): Task[] {
    return this.tasks;
  }

  //   getTasksWithFilters(filterDto: GetTaskFilteredDto): Task[] {
  //     const { status, search } = filterDto;

  //     let tasks = this.getAllTasks();

  //     if (status) {
  //       tasks = tasks.filter((task) => task.status === status);
  //     }
  //     if (search) {
  //       tasks = tasks.filter(
  //         (task) =>
  //           task.title.includes(search) || task.description.includes(search),
  //       );
  //     }
  //     return tasks;
  //   }

  //   getTaskById(id: string): Task {
  //     return this.tasks.find((task) => task.id === id);
  //   }

  async createTask(createTaskDto: CreateTaskDto): Promise<Task> {
    const { title, description } = createTaskDto;

    const task: Task = new this.TaskModel({
      title,
      description,
    });

    const result = await task.save();
    return result;
  }

  //   deleteTask(id: string): void {
  //     this.tasks = this.tasks.filter((task) => task.id !== id);
  //   }

  //   updateTaskStatus(id: string, status: TaskStatus): Task {
  //     const task = this.getTaskById(id);
  //     task.status = status;
  //     return task;
  //   }
}
